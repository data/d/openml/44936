# OpenML dataset: sgemm_gpu_kernel_performance

https://www.openml.org/d/44936

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

This data set measures the running time of a matrix-matrix product A\*B = C, where all matrices have size 2048 x 2048, using a parameterizable SGEMM GPU kernel with 241600 possible parameter combinations. For each tested combination, 4 runs were performed and their results are reported as the 4 last columns. All times are measured in milliseconds.

There are 14 parameter, the first 10 are ordinal and can only take up to 4 different powers of two values, and the 4 last variables are binary. Out of 1327104 total parameter combinations, only 241600 are feasible (due to various kernel constraints). This data set contains the results for all these feasible combinations.

The experiment was run on a desktop workstation running Ubuntu 16.04 Linux with an Intel Core i5 (3.5GHz), 16GB RAM, and a NVidia Geforce GTX 680 4GB GF580 GTX-1.5GB GPU. We use the 'gemm_fast' kernel from the automatic OpenCL kernel tuning library 'CLTune'.


**Attribute Description**

1-2. *MWG*, *NWG* - per-matrix 2D tiling at workgroup level: {16, 32, 64, 128} (integer)
3. *KWG* - inner dimension of 2D tiling at workgroup level: {16, 32} (integer)
4-5. *MDIMC*, *NDIMC* - local workgroup size: {8, 16, 32} (integer)
6-7. *MDIMA*, *NDIMB* - local memory shape: {8, 16, 32} (integer)
8. *KWI* - kernel loop unrolling factor: {2, 8} (integer)
9-10. *VWM*, *VWN* - per-matrix vector widths for loading and storing: {1, 2, 4, 8} (integer)
11-12. *STRM*, *STRN* - enable stride for accessing off-chip memory within a single thread: {0, 1} (categorical)
13-14. *SA*, *SB* - per-matrix manual caching of the 2D workgroup tile: {0, 1} (categorical)
15-18. *Run1*, *Run2*, *Run3*, *Run4* - performance times in milliseconds for 4 independent runs using the same parameters, ranging between 13.25 and 3397.08.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44936) of an [OpenML dataset](https://www.openml.org/d/44936). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44936/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44936/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44936/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

